# @zedtk/semantic-release-nuget

[**semantic-release**](https://github.com/semantic-release/semantic-release) plugin to publish a NuGet package.

| Step               | Description                                                |
| ------------------ | ---------------------------------------------------------- |
| `verifyConditions` | Verify the configuration.                                  |
| `prepare`          | Update the `.csproj` version and create the NuGet package. |
| `publish`          | Publish the NuGet package to the registry.                 |

## Install

```bash
$ npm install @zedtk/semantic-release-nuget -D
```

## Usage

The plugin can be configured in the [**semantic-release** configuration file](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#configuration):

```json
{
    "plugins": [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        "@zedtk/semantic-release-nuget"
    ]
}
```

## Configuration

### Environment variables

| Variable        | Description                    |
| --------------- | ------------------------------ |
| `NUGET_API_KEY` | API key for the NuGet registry |

### Options

| Options              | Description                                                                   | Default                               |
| -------------------- | ----------------------------------------------------------------------------- | ------------------------------------- |
| `publish`            | Whether to publish the `NuGet` package to the registry.                       | `true`                                |
| `projectRoot`        | Directory path to publish.                                                    | `.`                                   |
| `buildConfiguration` | Project build configuration.                                                  | `Release`                             |
| `includeSymbols`     | Whether to publish a symbols package (`.snupkg`).                             | `true`                                |
| `packageDir`         | Directory path in which to write the the package.                             | `dist`                                |
| `registry`           | Registry name (existing NuGet source), url or path to which push the package. | `https://api.nuget.org/v3/index.json` |

**Note**: If `publish` is `false` the `.csproj` version will still be updated.

**Note**: The `projectRoot` directory must contain a `.csproj`. The version will be updated only in the `.csproj` within the `projectRoot` directory.

**Note**: The `includeSymbols` option allows Source Link support. Some resources to help configure your project:

-   [Producing Packages with Source Link](https://devblogs.microsoft.com/dotnet/producing-packages-with-source-link)
-   [Using Source Link in .NET projects and how to configure Visual Studio to use it](https://lurumad.github.io/using-source-link-in-net-projects-and-how-to-configure-visual-studio-to-use-it)

**Note**: If you use a [shareable configuration](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/shareable-configurations.md#shareable-configurations) that defines one of these options you can set it to `false` in your [**semantic-release** configuration](https://github.com/semantic-release/semantic-release/blob/master/docs/usage/configuration.md#configuration) in order to use the default value.

### Examples

The `publish` and `packageDir` option can be used to skip the publishing to the `NuGet` registry and instead, release the package tarball with another plugin. For example with the [@semantic-release/github](https://github.com/semantic-release/github) plugin:

```json
{
    "plugins": [
        "@semantic-release/commit-analyzer",
        "@semantic-release/release-notes-generator",
        [
            "@zedtk/semantic-release-nuget",
            {
                "publish": false,
                "packageDir": "dist"
            }
        ],
        [
            "@semantic-release/github",
            {
                "assets": "dist/*.nupkg"
            }
        ]
    ]
}
```
