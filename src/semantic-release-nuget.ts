import { resolve } from "path";

import { name as npmPackageName } from "../package.json";
import {
    CsprojInfo,
    SemanticReleaseNuGetConfig,
    SemanticReleasePublishOptions,
    UnsafeSemanticReleaseNuGetConfig,
    VerifyConditionsContext,
    VerifyReleaseContext,
} from "./definitions/types";
import {
    ConfigurationService,
    CsprojService,
    ErrorService,
    NuGetService,
} from "./services";

// const enum SemanticReleaseNuGetState {
//     CREATED,
//     VERIFIED,
//     PREPARED,
// }

export class SemanticReleaseNuGet {
    // private state: SemanticReleaseNuGetState =
    //     SemanticReleaseNuGetState.CREATED;

    constructor(
        private readonly configurationService: ConfigurationService,
        private readonly csprojService: CsprojService,
        private readonly nuGetService: NuGetService
    ) {}

    public static build(): SemanticReleaseNuGet {
        const errorService: ErrorService = new ErrorService();
        const configurationService: ConfigurationService =
            new ConfigurationService(errorService);
        const csprojService: CsprojService = new CsprojService(errorService);
        const nuGetService: NuGetService = new NuGetService(csprojService);

        return new SemanticReleaseNuGet(
            configurationService,
            csprojService,
            nuGetService
        );
    }

    public async verifyConditions(
        unsafeConfig: UnsafeSemanticReleaseNuGetConfig,
        context: VerifyConditionsContext
    ): Promise<void> {
        let publishPluginConfig: UnsafeSemanticReleaseNuGetConfig = {};

        if (context.options.publish !== undefined) {
            publishPluginConfig =
                (context.options.publish.find(
                    (x) => x.path === npmPackageName
                ) as SemanticReleasePublishOptions | undefined) ?? {};
        }

        this.configurationService.merge(
            unsafeConfig,
            publishPluginConfig,
            ConfigurationService.defaultConfig
        );

        this.configurationService.verify(unsafeConfig);

        const config: SemanticReleaseNuGetConfig =
            unsafeConfig as SemanticReleaseNuGetConfig;

        this.configurationService.resolvePaths(config, context.cwd);

        await this.csprojService.get(config.projectRoot);

        // this.state = SemanticReleaseNuGetState.VERIFIED;
    }

    public async prepare(
        unsafeConfig: UnsafeSemanticReleaseNuGetConfig,
        context: VerifyReleaseContext
    ): Promise<void> {
        await this.verifyConditions(unsafeConfig, context);
        // if (this.state < SemanticReleaseNuGetState.PREPARED) {
        const config: SemanticReleaseNuGetConfig =
            unsafeConfig as SemanticReleaseNuGetConfig;

        await this.csprojService.updateVersion(
            config.projectRoot,
            context.nextRelease.version,
            context.logger
        );

        await this.nuGetService.pack(
            config.projectRoot,
            config.packageDir,
            config.buildConfiguration,
            config.includeSymbols,
            context
        );

        //     this.state = SemanticReleaseNuGetState.PREPARED;
        // }
    }

    public async publish(
        unsafeConfig: UnsafeSemanticReleaseNuGetConfig,
        context: VerifyReleaseContext
    ): Promise<boolean> {
        await this.verifyConditions(unsafeConfig, context);
        // await this.prepare(unsafeConfig, context);

        const config: SemanticReleaseNuGetConfig =
            unsafeConfig as SemanticReleaseNuGetConfig;

        if (!config.publish) {
            context.logger.log("Skip publishing to NuGet as publish is false");
            return false;
        }

        const csprojInfo: CsprojInfo = await this.csprojService.get(
            config.projectRoot
        );

        const packagePath: string = resolve(
            config.packageDir,
            this.nuGetService.getPackageName(
                csprojInfo.name,
                context.nextRelease.version
            )
        );

        await this.nuGetService.push(packagePath, config.registry, context);

        return true;
    }
}
