import { basename, resolve } from "path";

import execa, { ExecaChildProcess } from "execa";

import { CsprojInfo, ExecutionContext } from "../definitions/types";
import { CsprojService } from "./csproj.service";

export class NuGetService {
    constructor(private readonly csprojService: CsprojService) {}

    public async pack(
        projectRoot: string,
        packageDir: string,
        buildConfiguration: string,
        includeSymbols: boolean,
        { env, stdout, stderr, logger }: ExecutionContext
    ): Promise<string> {
        const csproj: CsprojInfo = await this.csprojService.get(projectRoot);
        const packageName: string = this.getPackageName(
            csproj.name,
            csproj.version
        );

        logger.log(`Creating ${packageName}`);

        let includeSymbolsArgs: Array<string> = [];
        if (includeSymbols) {
            includeSymbolsArgs = [
                "--include-symbols",
                "-p:SymbolPackageFormat=snupkg",
                "-p:ContinuousIntegrationBuild=true",
            ];
        }

        const dotnetPack: ExecaChildProcess<string> = execa(
            "dotnet",
            [
                "pack",
                "-c",
                buildConfiguration,
                "-o",
                packageDir,
                ...includeSymbolsArgs,
            ],
            { cwd: csproj.directory, env }
        );
        dotnetPack.stdout?.pipe(stdout, { end: false });
        dotnetPack.stderr?.pipe(stderr, { end: false });
        await dotnetPack;

        logger.log(`Created ${packageName}`);

        return resolve(packageDir, packageName);
    }

    // TODO : Add to readme
    // Push to directory :
    // dotnet nuget push "**/bin/Release/*.nupkg" -s C:/local-registry
    // Push to gitlab :
    // dotnet nuget add source "$CI_SERVER_URL/api/v4/projects/$CI_PROJECT_ID/packages/nuget/index.json" -n gitlab -u gitlab-ci-token -p $CI_JOB_TOKEN --store-password-in-clear-text
    // dotnet nuget push "**/bin/Release/*.nupkg" -s gitlab
    // Push to NuGet :
    // dotnet nuget push "**/bin/Release/*.nupkg" -s https://api.nuget.org/v3/index.json -k $NUGET_API_KEY
    public async push(
        packagePath: string,
        registry: string,
        { cwd, env, stdout, stderr, logger }: ExecutionContext
    ): Promise<void> {
        const packageFileName: string = basename(packagePath);

        logger.log(`Pushing ${packageFileName} to NuGet registry ${registry}`);

        let keyArgs: Array<string> = [];
        if (env.NUGET_API_KEY !== undefined) {
            keyArgs = ["-k", env.NUGET_API_KEY];
        }

        const result: ExecaChildProcess<string> = execa(
            "dotnet",
            ["nuget", "push", packagePath, "-s", registry, ...keyArgs],
            {
                cwd,
                env,
            }
        );
        result.stdout?.pipe(stdout, { end: false });
        result.stderr?.pipe(stderr, { end: false });
        await result;

        logger.log(`Pushed ${packageFileName} to NuGet registry ${registry}`);
    }

    public getPackageName(projectName: string, projectVersion: string): string {
        return `${projectName}.${projectVersion}.nupkg`;
    }
}
