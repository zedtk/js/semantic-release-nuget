import SemanticReleaseError from "@semantic-release/error";
import { constantCase } from "change-case";

import { homepage as npmPackageHomepage } from "../../package.json";
import { SemanticReleaseNuGetConfig } from "../definitions/types";

export class ErrorService {
    private static readonly EXPECTED_TYPES: Record<
        keyof SemanticReleaseNuGetConfig,
        string
    > = {
        buildConfiguration: "String",
        includeSymbols: "Boolean",
        packageDir: "String",
        projectRoot: "String",
        publish: "Boolean",
        registry: "String",
    };

    private static linkify(file: string): string {
        return `${npmPackageHomepage}/blob/master/${file}`;
    }

    public buildError(type: ErrorType): SemanticReleaseError {
        switch (type) {
            case "E_NO_CSPROJ":
                return new SemanticReleaseError(
                    "Missing `.csproj` file.",
                    type,
                    "A `.csproj` file at the root of your project is required to release on NuGet."
                );
            case "E_NO_CSPROJ_VERSION":
                return new SemanticReleaseError(
                    "Missing `Version` tag in `.csproj` file.",
                    type,
                    "A `Version` tag in the `.csproj` file is required."
                );
        }
    }

    public buildInvalidOptionError(
        option: keyof SemanticReleaseNuGetConfig,
        value: unknown
    ): SemanticReleaseError {
        return new SemanticReleaseError(
            `Invalid \`${option}\` option.`,
            `E_INVALID_OPTION_${constantCase(option)}`,
            `The [${option} option](${ErrorService.linkify(
                `README.md#options`
            )}), if defined, must be a \`${
                ErrorService.EXPECTED_TYPES[option]
            }\`.
            Your configuration for the \`${option}\` option is \`${value}\`.`
        );
    }
}

export type ErrorType = "E_NO_CSPROJ" | "E_NO_CSPROJ_VERSION";
