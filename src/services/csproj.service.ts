import { resolve } from "path";

import { outputFile, readFile } from "fs-extra";
import glob from "glob-promise";

import { CsprojInfo, Logger } from "../definitions/types";
import { ErrorService } from "./error.service";

export class CsprojService {
    private static readonly ENCODING: string = "utf8";
    private static readonly VERSION_REGEX: RegExp = /<Version>(.*)<\/Version>/;

    constructor(private readonly errorService: ErrorService) {}

    public async get(projectRoot: string): Promise<CsprojInfo> {
        const search: Array<string> = await glob("*.csproj", {
            cwd: projectRoot,
        });

        if (search.length === 0) {
            throw this.errorService.buildError("E_NO_CSPROJ");
        }

        const fileName: string = search[0];

        const path: string = resolve(projectRoot, fileName);
        const content: string = await readFile(path, CsprojService.ENCODING);
        const name: string = fileName.slice(0, -".csproj".length);

        const searchVersion: RegExpExecArray | null =
            CsprojService.VERSION_REGEX.exec(content);
        if (searchVersion === null) {
            // TODO : default version 0.0.0 ?
            throw this.errorService.buildError("E_NO_CSPROJ_VERSION");
        }
        const version: string = searchVersion[1];

        return {
            directory: projectRoot,
            fileName,
            path,
            content,
            name,
            version,
        };
    }

    public async updateVersion(
        projectRoot: string,
        nextReleaseVersion: string,
        logger: Logger
    ): Promise<void> {
        const csproj: CsprojInfo = await this.get(projectRoot);

        logger.log(`Updating ${csproj.path} to version ${nextReleaseVersion}`);

        const updatedContent: string = csproj.content.replace(
            CsprojService.VERSION_REGEX,
            `<Version>${nextReleaseVersion}</Version>`
        );

        await outputFile(csproj.path, updatedContent, CsprojService.ENCODING);

        logger.log(`Updated ${csproj.path} to version ${nextReleaseVersion}`);
    }
}
