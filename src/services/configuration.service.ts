import { resolve } from "path";

import SemanticReleaseError from "@semantic-release/error";
import AggregateError from "aggregate-error";

import {
    SemanticReleaseNuGetConfig,
    UnsafeSemanticReleaseNuGetConfig,
} from "../definitions/types";
import { ErrorService } from "./error.service";

export class ConfigurationService {
    public static readonly defaultConfig: Readonly<SemanticReleaseNuGetConfig> =
        Object.freeze({
            buildConfiguration: "Release",
            includeSymbols: true,
            packageDir: "dist",
            projectRoot: ".",
            publish: true,
            registry: "https://api.nuget.org/v3/index.json",
        });

    private static readonly VALIDATORS: Record<
        keyof SemanticReleaseNuGetConfig,
        (x: unknown) => boolean
    > = {
        buildConfiguration: ConfigurationService.isNonEmptyString,
        includeSymbols: (x) => typeof x === "boolean",
        packageDir: ConfigurationService.isNonEmptyString,
        projectRoot: ConfigurationService.isNonEmptyString,
        publish: (x) => typeof x === "boolean",
        registry: ConfigurationService.isNonEmptyString,
    };

    constructor(private readonly errorService: ErrorService) {}

    public static getConfigKeys(): Array<keyof SemanticReleaseNuGetConfig> {
        const configForKeys: Required<SemanticReleaseNuGetConfig> = {
            buildConfiguration: "",
            includeSymbols: true,
            packageDir: "",
            projectRoot: "",
            publish: true,
            registry: "",
        };
        return (
            Object.keys(configForKeys) as Array<
                keyof SemanticReleaseNuGetConfig
            >
        ).sort((a, b) => a.localeCompare(b));
    }

    private static isNonEmptyString(value: unknown): boolean {
        return typeof value === "string" && value.trim().length > 0;
    }

    public merge(
        target: UnsafeSemanticReleaseNuGetConfig,
        ...sources: Array<UnsafeSemanticReleaseNuGetConfig>
    ): void {
        for (const key of ConfigurationService.getConfigKeys()) {
            if (target[key] === undefined) {
                target[key] = sources.find(
                    (source) => source[key] !== undefined
                )?.[key];
            }
        }
    }

    public verify(config: UnsafeSemanticReleaseNuGetConfig): void {
        const errorsReturn: Array<SemanticReleaseError> =
            ConfigurationService.getConfigKeys()
                .filter((x) => !this.isOptionValid(x, config[x]))
                .map((x) =>
                    this.errorService.buildInvalidOptionError(x, config[x])
                );

        if (errorsReturn.length > 0) {
            throw new AggregateError(errorsReturn);
        }
    }

    public resolvePaths(config: SemanticReleaseNuGetConfig, cwd: string): void {
        config.packageDir = resolve(cwd, config.packageDir);
        config.projectRoot = resolve(cwd, config.projectRoot);
    }

    private isOptionValid(
        option: keyof SemanticReleaseNuGetConfig,
        value: unknown
    ): boolean {
        return ConfigurationService.VALIDATORS[option](value);
    }
}
