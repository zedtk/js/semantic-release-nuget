import {
    UnsafeSemanticReleaseNuGetConfig,
    VerifyConditionsContext,
    VerifyReleaseContext,
} from "./definitions/types";
import { SemanticReleaseNuGet } from "./semantic-release-nuget";

const semanticReleaseNuGet: SemanticReleaseNuGet = SemanticReleaseNuGet.build();

async function verifyConditions(
    unsafeConfig: UnsafeSemanticReleaseNuGetConfig,
    context: VerifyConditionsContext
): Promise<void> {
    await semanticReleaseNuGet.verifyConditions(unsafeConfig, context);
}

async function prepare(
    unsafeConfig: UnsafeSemanticReleaseNuGetConfig,
    context: VerifyReleaseContext
): Promise<void> {
    await semanticReleaseNuGet.prepare(unsafeConfig, context);
}

async function publish(
    unsafeConfig: UnsafeSemanticReleaseNuGetConfig,
    context: VerifyReleaseContext
): Promise<void> {
    await semanticReleaseNuGet.publish(unsafeConfig, context);
}

export = { verifyConditions, prepare, publish };
