export interface SemanticReleaseNuGetConfig {
    buildConfiguration: string;
    includeSymbols: boolean;
    packageDir: string;
    projectRoot: string;
    publish: boolean;
    registry: string;
}

export type UnsafeSemanticReleaseNuGetConfig = Partial<
    Record<keyof SemanticReleaseNuGetConfig, unknown>
>;

export type LogMethod = (
    message?: unknown,
    ...optionalArgs: Array<unknown>
) => void;

export interface Logger {
    log: LogMethod;
}

export interface ExecutionContext {
    cwd: string;
    env: NodeJS.ProcessEnv;
    stdout: NodeJS.WritableStream;
    stderr: NodeJS.WritableStream;
    logger: Logger;
}

export interface PublishOptions {
    [index: string]: unknown;
    path: string;
}

export interface SemanticReleasePublishOptions
    extends PublishOptions,
        UnsafeSemanticReleaseNuGetConfig {}

export interface ContextOptions {
    publish?: Array<PublishOptions | SemanticReleasePublishOptions>;
}

export interface VerifyConditionsContext extends ExecutionContext {
    options: ContextOptions;
}

export interface VerifyReleaseContext extends VerifyConditionsContext {
    nextRelease: { version: string };
}

export interface CsprojInfo {
    directory: string;
    fileName: string;
    path: string;
    content: string;
    name: string;
    version: string;
}
