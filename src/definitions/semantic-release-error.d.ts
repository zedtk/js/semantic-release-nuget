declare module "@semantic-release/error" {
    export default class SemanticReleaseError extends Error {
        public name: "SemanticReleaseError";
        public code: string;
        public details: string;
        public semanticRelease: true;
        constructor(message: string, code: string, details: string);
    }
}
