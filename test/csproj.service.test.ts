import "jest-extended";
import "./jest-matchers";

import { resolve } from "path";

import { outputFile } from "fs-extra";
import { WritableStreamBuffer } from "stream-buffers";
import { directory } from "tempy";

import {
    CsprojInfo,
    LogMethod,
    SemanticReleaseNuGetConfig,
} from "../src/definitions/types";
import {
    ErrorService,
    ConfigurationService,
    CsprojService,
} from "../src/services";
import { MockedExecutionContext } from "./tools";
import { buildCsproj } from "./tools/build-csproj";

describe("csproj service", () => {
    const errorService: ErrorService = new ErrorService();
    const configurationService: ConfigurationService = new ConfigurationService(
        errorService
    );
    const csprojService: CsprojService = new CsprojService(errorService);

    let config: SemanticReleaseNuGetConfig;
    let context: MockedExecutionContext;

    beforeEach(() => {
        const cwd: string = directory();

        config = { ...ConfigurationService.defaultConfig };
        configurationService.resolvePaths(config, cwd);

        context = {
            cwd,
            env: {},
            stdout: new WritableStreamBuffer(),
            stderr: new WritableStreamBuffer(),
            logger: {
                log: jest.fn<ReturnType<LogMethod>, Parameters<LogMethod>>(),
            },
        };
    });

    describe("get .csproj information (name, version, etc.)", () => {
        it("returns .csproj information", async () => {
            await outputFile(
                resolve(config.projectRoot, "test.csproj"),
                buildCsproj("0.0.0-dev")
            );

            const result: CsprojInfo = await csprojService.get(
                config.projectRoot
            );

            expect(result).toEqual({
                directory: config.projectRoot,
                fileName: "test.csproj",
                path: resolve(config.projectRoot, "test.csproj"),
                content: buildCsproj("0.0.0-dev"),
                name: "test",
                version: "0.0.0-dev",
            });
        });

        it("fails if .csproj is missing", async () => {
            try {
                await csprojService.get(config.projectRoot);
            } catch (error) {
                expect(error).toBeSemanticReleaseError("E_NO_CSPROJ");
            }
        });

        it("fails if .csproj is malformed", async () => {
            await outputFile(
                resolve(config.projectRoot, "test.csproj"),
                '<Project Sdk="Microsoft.NET.Sdk"></Project>'
            );

            try {
                await csprojService.get(config.projectRoot);
            } catch (error) {
                expect(error).toBeSemanticReleaseError("E_NO_CSPROJ_VERSION");
            }
        });
    });

    describe("update .csproj version", () => {
        it("updates .csproj version", async () => {
            const csprojPath: string = resolve(
                config.projectRoot,
                "test.csproj"
            );

            await outputFile(csprojPath, buildCsproj("0.0.0-dev"));

            const nextReleaseVersion: string = "1.0.0";

            await csprojService.updateVersion(
                config.projectRoot,
                nextReleaseVersion,
                context.logger
            );

            const result: CsprojInfo = await csprojService.get(
                config.projectRoot
            );
            expect(result.version).toBe(nextReleaseVersion);
        });

        it("preserves indentation and newline", async () => {
            await outputFile(
                resolve(config.projectRoot, "test.csproj"),
                buildCsproj("0.0.0-dev")
            );

            const nextReleaseVersion: string = "1.0.0";

            await csprojService.updateVersion(
                config.projectRoot,
                nextReleaseVersion,
                context.logger
            );

            const result: CsprojInfo = await csprojService.get(
                config.projectRoot
            );
            expect(result.content).toBe(buildCsproj(nextReleaseVersion));
        });

        it("logs correct information", async () => {
            const csprojPath: string = resolve(
                config.projectRoot,
                "test.csproj"
            );

            await outputFile(csprojPath, buildCsproj("0.0.0-dev"));

            const nextReleaseVersion: string = "1.0.0";

            await csprojService.updateVersion(
                config.projectRoot,
                nextReleaseVersion,
                context.logger
            );

            expect(context.logger.log.mock.calls[0][0]).toEqual(
                `Updating ${csprojPath} to version ${nextReleaseVersion}`
            );
            expect(context.logger.log.mock.calls[1][0]).toEqual(
                `Updated ${csprojPath} to version ${nextReleaseVersion}`
            );
        });
    });
});
