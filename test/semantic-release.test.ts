import "jest-extended";
import "./jest-matchers";

import { resolve } from "path";

import execa from "execa";
import { mkdir, pathExists } from "fs-extra";
import { outputFile } from "fs-extra";
import { directory } from "tempy";

import { applySemRel, commitAll } from "./test-env/common";
import { setupDotnetTestEnv } from "./test-env/dotnet";

describe("semantic-release", () => {
    it("uses nuget plugin", async () => {
        const registry: string = resolve(directory(), "nuget");
        await mkdir(registry);

        const gitRoot: string = await setupDotnetTestEnv({
            registry,
        });

        await outputFile(resolve(gitRoot, "init.txt"), "init content");
        await commitAll(gitRoot, "feat: initial commit");

        const initVersion: string = "1.0.0";
        const initSemRel: execa.ExecaReturnValue<string> = await applySemRel(
            gitRoot
        );
        expect(initSemRel.stdout).toContain(
            `There is no previous release, the next release version is ${initVersion}`
        );
        expect(initSemRel.stdout).toContain(`Created tag v${initVersion}`);
        expect(initSemRel.stdout).toContain(
            `Created MyProject.${initVersion}.nupkg`
        );
        expect(initSemRel.stdout).toContain(
            `Pushed MyProject.${initVersion}.nupkg to NuGet registry`
        );
        expect(
            await pathExists(
                resolve(registry, `MyProject.${initVersion}.nupkg`)
            )
        ).toBeTrue();
    }, 30000);
});
