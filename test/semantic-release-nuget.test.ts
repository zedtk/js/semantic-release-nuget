import "jest-extended";
import "./jest-matchers";

import { resolve } from "path";

import { mkdir, outputFile, pathExists } from "fs-extra";
import { WritableStreamBuffer } from "stream-buffers";
import { directory } from "tempy";

import { name as npmPackageName } from "../package.json";
import {
    LogMethod,
    SemanticReleaseNuGetConfig,
    UnsafeSemanticReleaseNuGetConfig,
} from "../src/definitions/types";
import { SemanticReleaseNuGet } from "../src/semantic-release-nuget";
import {
    ConfigurationService,
    CsprojService,
    ErrorService,
} from "../src/services";
import { buildCsproj, MockedExecutionContext } from "./tools";

describe("semantic-release-nuget", () => {
    const errorService: ErrorService = new ErrorService();
    const configurationService: ConfigurationService = new ConfigurationService(
        errorService
    );
    const csprojService: CsprojService = new CsprojService(errorService);

    const configKeys: Array<keyof SemanticReleaseNuGetConfig> =
        ConfigurationService.getConfigKeys();

    let context: MockedExecutionContext;

    let semanticReleaseNuGet: SemanticReleaseNuGet;

    beforeEach(() => {
        const cwd: string = directory();

        context = {
            cwd,
            env: {},
            stdout: new WritableStreamBuffer(),
            stderr: new WritableStreamBuffer(),
            logger: {
                log: jest.fn<ReturnType<LogMethod>, Parameters<LogMethod>>(),
            },
        };

        semanticReleaseNuGet = SemanticReleaseNuGet.build();
    });

    describe("verifies conditions", () => {
        const testPublishPluginConfig: SemanticReleaseNuGetConfig = {
            buildConfiguration: "publishPluginConfigBuildConfiguration",
            includeSymbols: true,
            packageDir: "publishPluginConfigPackageDir",
            projectRoot: "publishPluginConfigProjectRoot",
            publish: false,
            registry: "publishPluginConfigRegistry",
        };
        const testPluginConfig: SemanticReleaseNuGetConfig = {
            buildConfiguration: "pluginConfigBuildConfiguration",
            includeSymbols: true,
            packageDir: "pluginConfigPackageDir",
            projectRoot: "pluginConfigProjectRoot",
            publish: true,
            registry: "pluginConfigRegistry",
        };

        it("sets plugin configuration to default if no configuration is provided", async () => {
            await outputFile(
                resolve(context.cwd, "test.csproj"),
                buildCsproj("0.0.0-dev")
            );

            const pluginConfig: UnsafeSemanticReleaseNuGetConfig = {};

            await semanticReleaseNuGet.verifyConditions(pluginConfig, {
                ...context,
                options: {},
            });

            const expectedConfig: SemanticReleaseNuGetConfig = {
                ...ConfigurationService.defaultConfig,
            };
            configurationService.resolvePaths(expectedConfig, context.cwd);
            expect(pluginConfig).toEqual(expectedConfig);
        });

        describe("sets an option to context value if no plugin value is provided", () => {
            for (const key of configKeys) {
                test(`sets "${key}" option to context value if no plugin value is provided`, async () => {
                    const publishPluginConfig: Partial<SemanticReleaseNuGetConfig> =
                        {
                            [key]: testPublishPluginConfig[key],
                        };

                    await outputFile(
                        resolve(
                            context.cwd,
                            publishPluginConfig.projectRoot ?? "",
                            "test.csproj"
                        ),
                        buildCsproj("0.0.0-dev")
                    );

                    const pluginConfig: UnsafeSemanticReleaseNuGetConfig = {};

                    await semanticReleaseNuGet.verifyConditions(pluginConfig, {
                        ...context,
                        options: {
                            publish: [
                                {
                                    path: npmPackageName,
                                    ...publishPluginConfig,
                                },
                            ],
                        },
                    });

                    const expectedConfig: SemanticReleaseNuGetConfig = {
                        ...ConfigurationService.defaultConfig,
                        ...publishPluginConfig,
                    };
                    configurationService.resolvePaths(
                        expectedConfig,
                        context.cwd
                    );
                    expect(pluginConfig).toEqual(expectedConfig);
                });
            }
        });

        describe("sets an option to plugin value if provided", () => {
            for (const key of configKeys) {
                test(`sets "${key}" option to plugin value if provided`, async () => {
                    const pluginConfig: Partial<SemanticReleaseNuGetConfig> = {
                        [key]: testPluginConfig[key],
                    };

                    await outputFile(
                        resolve(
                            context.cwd,
                            pluginConfig.projectRoot ?? "",
                            "test.csproj"
                        ),
                        buildCsproj("0.0.0-dev")
                    );

                    await semanticReleaseNuGet.verifyConditions(pluginConfig, {
                        ...context,
                        options: {
                            publish: [
                                {
                                    path: npmPackageName,
                                    [key]: testPublishPluginConfig[key],
                                },
                            ],
                        },
                    });

                    const expectedConfig: SemanticReleaseNuGetConfig = {
                        ...ConfigurationService.defaultConfig,
                        ...pluginConfig,
                    };
                    configurationService.resolvePaths(
                        expectedConfig,
                        context.cwd
                    );
                    expect(pluginConfig).toEqual(expectedConfig);
                });
            }
        });
    });

    it("prepares", async () => {
        await outputFile(
            resolve(context.cwd, "test.csproj"),
            buildCsproj("0.0.0-dev")
        );

        const nextReleaseVersion: string = "1.0.0";

        await semanticReleaseNuGet.prepare(
            {
                projectRoot: ".",
            },
            {
                ...context,
                options: {},
                nextRelease: { version: nextReleaseVersion },
            }
        );

        expect((await csprojService.get(context.cwd)).version).toBe(
            nextReleaseVersion
        );
    });

    it("publishes", async () => {
        const projectName: string = "test";

        await outputFile(
            resolve(context.cwd, `${projectName}.csproj`),
            buildCsproj("0.0.0-dev")
        );

        const registry: string = resolve(context.cwd, "nuget");
        await mkdir(registry);

        const nextReleaseVersion: string = "1.0.0";

        await semanticReleaseNuGet.prepare(
            {
                projectRoot: ".",
                registry,
            },
            {
                ...context,
                options: {},
                nextRelease: { version: nextReleaseVersion },
            }
        );

        expect(
            await semanticReleaseNuGet.publish(
                {
                    projectRoot: ".",
                    registry,
                },
                {
                    ...context,
                    options: {},
                    nextRelease: { version: nextReleaseVersion },
                }
            )
        ).toBeTrue();

        expect(
            await pathExists(
                resolve(registry, `${projectName}.${nextReleaseVersion}.nupkg`)
            )
        ).toBeTrue();
    });

    test('skips publication ("publish" is false)', async () => {
        const projectName: string = "test";

        await outputFile(
            resolve(context.cwd, `${projectName}.csproj`),
            buildCsproj("0.0.0-dev")
        );

        const registry: string = resolve(context.cwd, "nuget");
        await mkdir(registry);

        const nextReleaseVersion: string = "1.0.0";

        expect(
            await semanticReleaseNuGet.publish(
                {
                    projectRoot: ".",
                    publish: false,
                    registry,
                },
                {
                    ...context,
                    options: {},
                    nextRelease: { version: nextReleaseVersion },
                }
            )
        ).toBeFalse();

        expect(
            await pathExists(
                resolve(registry, `${projectName}.${nextReleaseVersion}.nupkg`)
            )
        ).toBeFalse();
    });

    it("verifies conditions, prepares and publishes", async () => {
        const projectName: string = "test";

        await outputFile(
            resolve(context.cwd, `${projectName}.csproj`),
            buildCsproj("0.0.0-dev")
        );

        const registry: string = resolve(context.cwd, "nuget");
        await mkdir(registry);

        const config: UnsafeSemanticReleaseNuGetConfig = {
            projectRoot: ".",
            registry,
        };

        await semanticReleaseNuGet.verifyConditions(config, {
            ...context,
            options: {},
        });

        const nextReleaseVersion: string = "1.0.0";

        await semanticReleaseNuGet.prepare(config, {
            ...context,
            options: {},
            nextRelease: { version: nextReleaseVersion },
        });

        expect(
            await semanticReleaseNuGet.publish(config, {
                ...context,
                options: {},
                nextRelease: { version: nextReleaseVersion },
            })
        ).toBeTrue();

        expect(
            await pathExists(
                resolve(registry, `${projectName}.${nextReleaseVersion}.nupkg`)
            )
        ).toBeTrue();
    });
});
