import { resolve } from "path";

import { outputJson, outputFile } from "fs-extra";

import { SemanticReleaseNuGetConfig } from "src/definitions/types";

import { pushAll, setupGit } from "./common";

export async function setupDotnetTestEnv(
    nugetConfig: Partial<SemanticReleaseNuGetConfig>
): Promise<string> {
    const gitRoot: string = await setupGit();

    await outputJson(resolve(gitRoot, ".releaserc.json"), {
        plugins: [
            "@semantic-release/commit-analyzer",
            "@semantic-release/release-notes-generator",
            [resolve(process.cwd(), "dist", "src", "index"), nugetConfig],
        ],
    });
    await outputFile(
        resolve(gitRoot, "MyProject.csproj"),
        `<Project Sdk="Microsoft.NET.Sdk">
<PropertyGroup>
  <TargetFrameworks>net5.0</TargetFrameworks>
  <LangVersion>9</LangVersion>
</PropertyGroup>
<PropertyGroup>
  <Version>1.0.0</Version>
</PropertyGroup>
</Project>`
    );
    await pushAll(gitRoot, "chore: init MyProject");

    return gitRoot;
}
