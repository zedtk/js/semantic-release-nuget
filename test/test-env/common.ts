import { resolve } from "path";

import execa from "execa";
import { directory } from "tempy";

const gitRepoName: string = "workspace";

export async function createOrigin(): Promise<string> {
    const originDirectory: string = resolve(directory(), `${gitRepoName}.git`);
    await execa("git", ["init", "--bare", originDirectory]);
    return originDirectory;
}

export async function clone(gitRepoUrl: string): Promise<string> {
    const gitRoot: string = resolve(directory(), gitRepoName);
    await execa("git", ["clone", gitRepoUrl, gitRoot]);
    await execa("git", ["config", "user.email", "test@test.com"], {
        cwd: gitRoot,
    });
    await execa("git", ["config", "user.name", "Test User"], { cwd: gitRoot });
    return gitRoot;
}

export async function setupGit(): Promise<string> {
    const originDirectory: string = await createOrigin();
    const gitRepoUrl: string = `file://${originDirectory}`;
    const gitRoot: string = await clone(gitRepoUrl);
    return gitRoot;
}

export async function commitAll(
    gitRoot: string,
    message: string
): Promise<string> {
    await execa("git", ["add", "-A"], { cwd: gitRoot });
    await execa("git", ["commit", "-m", message], { cwd: gitRoot });
    const { stdout } = await execa("git", ["rev-parse", "--verify", "HEAD"], {
        cwd: gitRoot,
    });
    return stdout;
}

export async function pushAll(gitRoot: string, message: string): Promise<void> {
    await commitAll(gitRoot, message);
    await execa("git", ["push", "origin", "master"], {
        cwd: gitRoot,
    });
}

export async function applySemRel(
    projectRoot: string
): Promise<execa.ExecaReturnValue<string>> {
    return execa("npx", ["semantic-release", "--no-ci", "--debug"], {
        cwd: projectRoot,
        env: {
            GITLAB_CI: undefined,
        },
    });
}
