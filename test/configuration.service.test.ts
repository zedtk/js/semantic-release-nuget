import "jest-extended";
import "jest-chain";
import "./jest-matchers";

import { resolve } from "path";

import AggregateError from "aggregate-error";
import { constantCase } from "change-case";
import { directory } from "tempy";

import {
    SemanticReleaseNuGetConfig,
    UnsafeSemanticReleaseNuGetConfig,
} from "../src/definitions/types";
import { ConfigurationService, ErrorService } from "../src/services";

describe("configuration service", () => {
    const errorService: ErrorService = new ErrorService();
    const configurationService: ConfigurationService = new ConfigurationService(
        errorService
    );

    const configKeys: Array<keyof SemanticReleaseNuGetConfig> =
        ConfigurationService.getConfigKeys();

    const testPluginConfig: SemanticReleaseNuGetConfig = {
        buildConfiguration: "pluginConfigBuildConfiguration",
        includeSymbols: true,
        packageDir: "pluginConfigPackageDir",
        projectRoot: "pluginConfigProjectRoot",
        publish: true,
        registry: "pluginConfigRegistry",
    };

    it("gives the list of configuration keys", () => {
        expect(ConfigurationService.getConfigKeys())
            .toBeArrayOfSize(6)
            .toContain<keyof SemanticReleaseNuGetConfig>("buildConfiguration")
            .toContain<keyof SemanticReleaseNuGetConfig>("includeSymbols")
            .toContain<keyof SemanticReleaseNuGetConfig>("packageDir")
            .toContain<keyof SemanticReleaseNuGetConfig>("projectRoot")
            .toContain<keyof SemanticReleaseNuGetConfig>("publish")
            .toContain<keyof SemanticReleaseNuGetConfig>("registry");
    });

    it("merges configurations into target", () => {
        const targetConfig: UnsafeSemanticReleaseNuGetConfig = {
            packageDir: "dir",
        };

        configurationService.merge(
            targetConfig,
            { projectRoot: "test" },
            { projectRoot: "test2", publish: false },
            ConfigurationService.defaultConfig
        );

        expect(targetConfig).toEqual({
            ...ConfigurationService.defaultConfig,
            packageDir: targetConfig.packageDir,
            projectRoot: "test",
            publish: false,
        });
    });

    describe("verifies the config", () => {
        it("succeeds on well formed config", () => {
            expect(() =>
                configurationService.verify({
                    buildConfiguration: "Release",
                    includeSymbols: true,
                    packageDir: "dist",
                    projectRoot: "test",
                    publish: true,
                    registry: "gitlab",
                })
            ).not.toThrow();
        });

        describe("throws an error if an option is not provided", () => {
            for (const key of configKeys) {
                test(`throws an error if "${key}" option is not provided`, () => {
                    try {
                        const pluginConfig: UnsafeSemanticReleaseNuGetConfig =
                            configKeys
                                .filter((x) => x !== key)
                                .reduce(
                                    (prevConfig, currKey) => ({
                                        ...prevConfig,
                                        [currKey]: testPluginConfig[currKey],
                                    }),
                                    {} as UnsafeSemanticReleaseNuGetConfig
                                );
                        configurationService.verify({
                            ...pluginConfig,
                            [key]: 12,
                        });
                    } catch (error) {
                        expect(error).toBeInstanceOf(AggregateError);
                        const errors: Array<unknown> = Array.from(
                            error as AggregateError
                        );
                        expect(errors).toBeArrayOfSize(1);
                        expect(errors[0]).toBeSemanticReleaseError(
                            `E_INVALID_OPTION_${constantCase(key)}`
                        );
                    }
                });
            }
        });

        it("throws an error if multiple options are not provided", () => {
            try {
                configurationService.verify({});
            } catch (error) {
                expect(error).toBeInstanceOf(AggregateError);
                const errors: Array<unknown> = Array.from(
                    error as AggregateError
                );
                expect(errors).toBeArrayOfSize(6);
            }
        });

        describe("throws an error if an option is not valid", () => {
            for (const key of configKeys) {
                test(`throws an error if "${key}" option is not valid`, () => {
                    try {
                        configurationService.verify({
                            ...testPluginConfig,
                            [key]: 12,
                        });
                    } catch (error) {
                        expect(error).toBeInstanceOf(AggregateError);
                        const errors: Array<unknown> = Array.from(
                            error as AggregateError
                        );
                        expect(errors).toBeArrayOfSize(1);
                        expect(errors[0]).toBeSemanticReleaseError(
                            `E_INVALID_OPTION_${constantCase(key)}`
                        );
                    }
                });
            }
        });

        it("throws an error if multiple options are not valid", () => {
            try {
                configurationService.verify({
                    buildConfiguration: 12,
                    includeSymbols: 12,
                    packageDir: 12,
                    projectRoot: 12,
                    publish: 12,
                    registry: 12,
                });
            } catch (error) {
                expect(error).toBeInstanceOf(AggregateError);
                const errors: Array<unknown> = Array.from(
                    error as AggregateError
                );
                expect(errors).toBeArrayOfSize(6);
            }
        });
    });

    it("resolves path of concerned options", () => {
        const config: SemanticReleaseNuGetConfig = {
            buildConfiguration: "Release",
            includeSymbols: true,
            packageDir: "dist",
            projectRoot: "test",
            publish: true,
            registry: "gitlab",
        };
        const cwd: string = directory();

        configurationService.resolvePaths(config, cwd);

        expect(config).toEqual({
            ...config,
            packageDir: resolve(cwd, "dist"),
            projectRoot: resolve(cwd, "test"),
        });
    });
});
