export function buildCsproj(version: string): string {
    return `<Project Sdk="Microsoft.NET.Sdk">
    <PropertyGroup>
        <TargetFrameworks>net5.0;netcoreapp3.1</TargetFrameworks>
        <LangVersion>9</LangVersion>
    </PropertyGroup>
    <PropertyGroup>
        <Version>${version}</Version>
    </PropertyGroup>
</Project>`;
}
