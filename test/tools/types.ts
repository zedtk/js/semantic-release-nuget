import { ExecutionContext, LogMethod } from "src/definitions/types";

export interface MockedExecutionContext extends ExecutionContext {
    logger: {
        log: jest.Mock<ReturnType<LogMethod>, Parameters<LogMethod>>;
    };
}
