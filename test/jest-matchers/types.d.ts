/// <reference types="jest" />

declare namespace jest {
    interface Matchers<R> {
        /**
         * Use `.toBeSemanticReleaseError` when checking if a value is a `SemanticReleaseError`.
         */
        toBeSemanticReleaseError(expectedCode: string): R;
    }
}
