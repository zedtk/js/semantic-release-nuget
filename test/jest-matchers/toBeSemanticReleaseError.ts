import SemanticReleaseError from "@semantic-release/error";
import diff from "jest-diff";
import { matcherHint, printExpected, printReceived } from "jest-matcher-utils";

const toBeSemanticReleaseError: jest.CustomMatcher = (
    received: unknown,
    expectedCode: string
) => {
    let pass: boolean = false;
    let message: jest.CustomMatcherResult["message"];

    if (received instanceof SemanticReleaseError) {
        if (received.code === expectedCode) {
            pass = true;
            message = () =>
                matcherHint("toBeSemanticReleaseError") +
                "\n\n" +
                "Expected: not a SemanticReleaseError\n" +
                `Received: ${printReceived(received)}`;
        } else {
            message = () => {
                const diffString: string | null = diff(
                    expectedCode,
                    received.code
                );
                return (
                    matcherHint("toBeSemanticReleaseError") +
                    "\n\n" +
                    (diffString?.includes("- Expected") !== undefined
                        ? `Difference:\n\n${diffString}`
                        : `Expected: ${printExpected(expectedCode)}\n` +
                          `Received: ${printReceived(received.code)}`)
                );
            };
        }
    } else {
        message = () =>
            matcherHint("toBeSemanticReleaseError") +
            "\n\n" +
            "Expected: a SemanticReleaseError\n" +
            `Received: ${printReceived(received)}`;
    }

    return { pass, message };
};

expect.extend({
    toBeSemanticReleaseError,
});
