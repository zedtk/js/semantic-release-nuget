import "jest-extended";
import "./jest-matchers";

import { basename, resolve } from "path";

import { mkdir, outputFile, pathExists } from "fs-extra";
import { WritableStreamBuffer } from "stream-buffers";
import { directory } from "tempy";

import {
    LogMethod,
    SemanticReleaseNuGetConfig,
} from "../src/definitions/types";
import {
    ErrorService,
    ConfigurationService,
    CsprojService,
    NuGetService,
} from "../src/services";
import { buildCsproj } from "./tools/build-csproj";
import { MockedExecutionContext } from "./tools/types";

describe("nuget service", () => {
    const errorService: ErrorService = new ErrorService();
    const configurationService: ConfigurationService = new ConfigurationService(
        errorService
    );
    const csprojService: CsprojService = new CsprojService(errorService);
    const nuGetService: NuGetService = new NuGetService(csprojService);

    let config: SemanticReleaseNuGetConfig;
    let context: MockedExecutionContext;

    beforeEach(() => {
        const cwd: string = directory();

        config = { ...ConfigurationService.defaultConfig };
        configurationService.resolvePaths(config, cwd);

        context = {
            cwd,
            env: {},
            stdout: new WritableStreamBuffer(),
            stderr: new WritableStreamBuffer(),
            logger: {
                log: jest.fn<ReturnType<LogMethod>, Parameters<LogMethod>>(),
            },
        };
    });

    it("gets a package name based on projet's name and version", () => {
        const projectName: string = "test";
        const projectVersion: string = "0.0.0-dev";

        const packageName: string = nuGetService.getPackageName(
            projectName,
            projectVersion
        );

        expect(packageName).toBe(`${projectName}.${projectVersion}.nupkg`);
    });

    describe("pack a NuGet package", () => {
        it("creates the package", async () => {
            const projectName: string = "test";
            const projectVersion: string = "0.0.0-dev";

            await outputFile(
                resolve(config.projectRoot, `${projectName}.csproj`),
                buildCsproj(projectVersion)
            );

            await nuGetService.pack(
                config.projectRoot,
                config.packageDir,
                config.buildConfiguration,
                config.includeSymbols,
                context
            );

            expect(
                await pathExists(
                    resolve(
                        config.packageDir,
                        `${projectName}.${projectVersion}.nupkg`
                    )
                )
            ).toBeTrue();
            expect(
                await pathExists(
                    resolve(
                        config.packageDir,
                        `${projectName}.${projectVersion}.snupkg`
                    )
                )
            ).toBeTrue();
        });

        it("returns the package path", async () => {
            const projectName: string = "test";
            const projectVersion: string = "0.0.0-dev";

            await outputFile(
                resolve(config.projectRoot, `${projectName}.csproj`),
                buildCsproj(projectVersion)
            );

            expect(
                await nuGetService.pack(
                    config.projectRoot,
                    config.packageDir,
                    config.buildConfiguration,
                    config.includeSymbols,
                    context
                )
            ).toBe(
                resolve(
                    config.packageDir,
                    `${projectName}.${projectVersion}.nupkg`
                )
            );
        });

        it("logs correct information", async () => {
            await outputFile(
                resolve(config.projectRoot, "test.csproj"),
                buildCsproj("0.0.0-dev")
            );

            const packagePath: string = await nuGetService.pack(
                config.projectRoot,
                config.packageDir,
                config.buildConfiguration,
                config.includeSymbols,
                context
            );

            expect(context.logger.log.mock.calls[0][0]).toEqual(
                `Creating ${basename(packagePath)}`
            );
            expect(context.logger.log.mock.calls[1][0]).toEqual(
                `Created ${basename(packagePath)}`
            );
        });
    });

    describe("publish a NuGet package", () => {
        it("publishes the package", async () => {
            const projectName: string = "test";
            const projectVersion: string = "0.0.0-dev";

            await outputFile(
                resolve(config.projectRoot, `${projectName}.csproj`),
                buildCsproj(projectVersion)
            );

            const packagePath: string = await nuGetService.pack(
                config.projectRoot,
                config.packageDir,
                config.buildConfiguration,
                config.includeSymbols,
                context
            );

            const registry: string = resolve(context.cwd, "nuget");
            await mkdir(registry);

            await nuGetService.push(packagePath, registry, context);

            expect(
                await pathExists(
                    resolve(registry, `${projectName}.${projectVersion}.nupkg`)
                )
            ).toBeTrue();
        });

        it("logs correct information", async () => {
            await outputFile(
                resolve(config.projectRoot, "test.csproj"),
                buildCsproj("0.0.0-dev")
            );

            const packagePath: string = await nuGetService.pack(
                config.projectRoot,
                config.packageDir,
                config.buildConfiguration,
                config.includeSymbols,
                context
            );

            const registry: string = resolve(context.cwd, "nuget");
            await mkdir(registry);

            await nuGetService.push(packagePath, registry, context);

            expect(context.logger.log.mock.calls[2][0]).toEqual(
                `Pushing ${basename(packagePath)} to NuGet registry ${registry}`
            );
            expect(context.logger.log.mock.calls[3][0]).toEqual(
                `Pushed ${basename(packagePath)} to NuGet registry ${registry}`
            );
        });
    });
});
